import numpy as np
import pygame
import sys
import math
 
BLUE = (0,0,255)
BLACK = (0,0,0)
RED = (255,0,0)
YELLOW = (255,255,0)
GREEN = (0,128,0)
WHITE = (255,255,255)
 
ROW_COUNT = 6
COLUMN_COUNT = 7
 
def create_board():
    board = np.zeros((ROW_COUNT,COLUMN_COUNT))
    return board
 
def drop_chip(board, row, col, chip):
    board[row][col] = chip
 
def is_valid_location(board, col):
    if col >= 0 and col <= 6:
        return board[ROW_COUNT-1][col] == 0
    else:
        return False
 
def get_next_open_row(board, col):
    for r in range(ROW_COUNT):
        if board[r][col] == 0:
            return r
 
# def print_board(board):
#     print(np.flip(board, 0))
 
def winning_move(board, chip):
    # Check horizontal locations for win
    for c in range(COLUMN_COUNT-3):
        for r in range(ROW_COUNT):
            if board[r][c] == chip and board[r][c+1] == chip and board[r][c+2] == chip and board[r][c+3] == chip:
                return True
 
    # Check vertical locations for win
    for c in range(COLUMN_COUNT):
        for r in range(ROW_COUNT-3):
            if board[r][c] == chip and board[r+1][c] == chip and board[r+2][c] == chip and board[r+3][c] == chip:
                return True
 
    # Check positively sloped diaganols
    for c in range(COLUMN_COUNT-3):
        for r in range(ROW_COUNT-3):
            if board[r][c] == chip and board[r+1][c+1] == chip and board[r+2][c+2] == chip and board[r+3][c+3] == chip:
                return True
 
    # Check negatively sloped diaganols
    for c in range(COLUMN_COUNT-3):
        for r in range(3, ROW_COUNT):
            if board[r][c] == chip and board[r-1][c+1] == chip and board[r-2][c+2] == chip and board[r-3][c+3] == chip:
                return True
 
def draw_board(board):
    for c in range(COLUMN_COUNT):
        for r in range(ROW_COUNT):
            pygame.draw.rect(screen, BLUE, (c*SQUARESIZE+SQUARESIZE, height-(r*SQUARESIZE+SQUARESIZE+SQUARESIZE), SQUARESIZE, SQUARESIZE))
            pygame.draw.circle(screen, BLACK, (int(c*SQUARESIZE+SQUARESIZE+SQUARESIZE/2), height-int(r*SQUARESIZE+SQUARESIZE+SQUARESIZE/2)), RADIUS)
            if board[r][c] == 1:
                pygame.draw.circle(screen, RED, (int(c*SQUARESIZE+SQUARESIZE+SQUARESIZE/2), height-int(r*SQUARESIZE+SQUARESIZE+SQUARESIZE/2)), RADIUS)
            elif board[r][c] == 2: 
                pygame.draw.circle(screen, YELLOW, (int(c*SQUARESIZE+SQUARESIZE+SQUARESIZE/2), height-int(r*SQUARESIZE+SQUARESIZE+SQUARESIZE/2)), RADIUS)
            #print_board(board)
    pygame.display.update()

def play_again():
    pygame.draw.rect(screen, BLACK, (0,0, width, SQUARESIZE))
    label = MDfont.render("Do you want to play again? y/n", 1, WHITE)
    screen.blit(label, (40,10))
    pygame.display.update()
    while True:
        user_input = input("Do you want to play again? y/n")
        user_input = user_input.lower()
        if user_input[0] == "y" or user_input[0] == "Y":
            return True
        elif user_input[0] == "n" or user_input[0] == "N":
            return False
        else:
            print("Invalid Entry")
            label = MDfont.render("Invalid Entry", 1, RED)
            screen.blit(label, (COLUMN_COUNT*SQUARESIZE/2.5,(ROW_COUNT+1)*SQUARESIZE+SQUARESIZE/2))
            pygame.display.update()
            pygame.time.wait(1000)
            pygame.draw.rect(screen, BLACK, (0,(ROW_COUNT+1)*SQUARESIZE, width, SQUARESIZE))
            pygame.display.update()


 
# board = create_board()
want_to_play = True
game_over = False
turn = 0
 
#initalize pygame
pygame.init()
 
#define our screen size
SQUARESIZE = 75
BUTTONSIZE = 75
 
#define width and height of board
width = (COLUMN_COUNT+2) * SQUARESIZE
height = (ROW_COUNT+2) * SQUARESIZE
 
size = (width, height)
 
RADIUS = int(SQUARESIZE/2 - 5)
 
screen = pygame.display.set_mode(size)
#Calling function draw_board again
# draw_board(board)
 
LGfont = pygame.font.SysFont("monospace", 45)
MDfont = pygame.font.SysFont("monospace", 25)
SMfont = pygame.font.SysFont("monospace", 15)

while want_to_play:
    board = create_board()
    draw_board(board)
    while not game_over:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
     
            if event.type == pygame.MOUSEMOTION:
                pygame.draw.rect(screen, BLACK, (0,0, width, SQUARESIZE))
                posx = event.pos[0]
                if posx >= 100 and posx <= (width-SQUARESIZE):
                    if turn == 0:
                        pygame.draw.circle(screen, RED, (posx, int(SQUARESIZE/2)), RADIUS)
                    else: 
                        pygame.draw.circle(screen, YELLOW, (posx, int(SQUARESIZE/2)), RADIUS)
            pygame.display.update()
     
            if event.type == pygame.MOUSEBUTTONDOWN:
                pygame.draw.rect(screen, BLACK, (0,0, width, SQUARESIZE))

                posx = event.pos[0]
                col = int(math.floor((posx-SQUARESIZE)/SQUARESIZE))

                if is_valid_location(board, col):
                    row = get_next_open_row(board, col)
                    if turn == 0:
                        drop_chip(board, row, col, 1)
                    else:
                        drop_chip(board, row, col, 2)
                    draw_board(board)

                    if winning_move(board, turn+1):
                        if turn == 0:
                            color = ["Red", RED]
                        else:
                            color = ["Yellow", YELLOW]
                        label = LGfont.render(f"{color[0]} wins!!", 1, color[1])
                        screen.blit(label, (40,10))
                        pygame.display.update()
                        pygame.time.wait(3000)
                        game_over = True
     
                    draw_board(board)
     
                    turn += 1
                    turn = turn % 2
    if game_over:
        if play_again():
            game_over = False
            want_to_play = True
        else:
            want_to_play = False
            pygame.time.wait(500)
            pygame.draw.rect(screen, BLACK, (0,0, width, SQUARESIZE))
            label = MDfont.render("Refresh the page to play again.", 1, WHITE)
            screen.blit(label, (40,10))
            pygame.display.update()
            pygame.time.wait(3000)
